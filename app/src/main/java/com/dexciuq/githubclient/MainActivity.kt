package com.dexciuq.githubclient

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dexciuq.githubclient.adapter.RepositoryAdapter
import com.dexciuq.githubclient.databinding.ActivityMainBinding
import com.dexciuq.githubclient.model.Repository
import com.dexciuq.githubclient.util.NetworkUtils
import com.dexciuq.githubclient.viewmodel.MainViewModel
import com.dexciuq.githubclient.viewmodel.NetworkViewModelFactory

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: RepositoryAdapter
    private val viewModel: MainViewModel by viewModels {
        NetworkViewModelFactory (
            onStart = { binding.progressBar.visibility = View.VISIBLE },
            isNetworkAvailable = { NetworkUtils.isNetworkAvailable(applicationContext) },
            onErrorAction = { Toast.makeText(this, it, Toast.LENGTH_SHORT).show() },
            onEnd = { binding.progressBar.visibility = View.GONE }
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        adapter = RepositoryAdapter(::startDetailsActivity)

        setContentView(binding.root)

        val recyclerView = binding.reposRecyclerView
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        viewModel.organizationRepos.observe(this, adapter::submitList)
        binding.searchButton.setOnClickListener {
            viewModel.getOrganizationRepos(binding.editText.text.toString())
        }
    }

    private fun startDetailsActivity(repository: Repository) {
        val intent = Intent(this, DetailsActivity::class.java)
        intent.putExtra(KEY_REPOSITORY, repository.name)
        intent.putExtra(KEY_ORGANIZATION, viewModel.organization.value)
        startActivity(intent)
    }

    companion object {
        const val KEY_REPOSITORY = "repository"
        const val KEY_ORGANIZATION = "organization"
    }
}