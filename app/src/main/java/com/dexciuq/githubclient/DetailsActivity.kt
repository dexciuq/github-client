package com.dexciuq.githubclient

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.dexciuq.githubclient.databinding.ActivityDetailsBinding
import com.dexciuq.githubclient.util.NetworkUtils
import com.dexciuq.githubclient.viewmodel.DetailsViewModel
import com.dexciuq.githubclient.viewmodel.NetworkViewModelFactory

class DetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailsBinding
    private val viewModel: DetailsViewModel by viewModels {
        NetworkViewModelFactory (
            onStart = { binding.progressBar.visibility = View.VISIBLE },
            isNetworkAvailable = { NetworkUtils.isNetworkAvailable(applicationContext) },
            onErrorAction = { Toast.makeText(this, it, Toast.LENGTH_SHORT).show() },
            onEnd = { binding.progressBar.visibility = View.GONE }
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_details)

        intent.extras?.let {
            val repository = it.getString(MainActivity.KEY_REPOSITORY)!!
            val organization = it.getString(MainActivity.KEY_ORGANIZATION)!!
            viewModel.getRepository(organization, repository)
        }

        viewModel.repository.observe(this) { repository ->
            binding.repository = repository
            listOf(binding.name, binding.forks, binding.watchers, binding.issues).forEach {
                it.visibility = View.VISIBLE
            }
        }
    }
}