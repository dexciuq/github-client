package com.dexciuq.githubclient.model

import com.google.gson.annotations.SerializedName

data class Repository (
    val name: String,
    val description: String?,
    @SerializedName("forks_count") val forks: Int,
    @SerializedName("watchers_count") val watchers: Int,
    @SerializedName("open_issues_count") val issues: Int,
    @SerializedName("parent") val parentRepository: ParentRepository?
)