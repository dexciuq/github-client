package com.dexciuq.githubclient.model

import com.google.gson.annotations.SerializedName

data class ParentRepository(
    @SerializedName("full_name") val name: String
)
