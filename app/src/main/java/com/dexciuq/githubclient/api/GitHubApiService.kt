package com.dexciuq.githubclient.api

import com.dexciuq.githubclient.model.Repository
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface GitHubApiService {

    @GET("orgs/{org}/repos")
    suspend fun getOrganizationRepos(@Path("org") org: String): List<Repository>

    @GET("repos/{org}/{repo}")
    suspend fun getRepository(@Path("org") org: String, @Path("repo") repo: String): Repository
}