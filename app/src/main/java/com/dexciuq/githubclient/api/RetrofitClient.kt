package com.dexciuq.githubclient.api

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {

    private const val BASE_URL = "https://api.github.com/"
    private const val TOKEN = "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"

    private val instance: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(OkHttpClient.Builder().addInterceptor(AuthInterceptor(TOKEN)).build())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val gitHubApiService: GitHubApiService = instance.create(GitHubApiService::class.java)
}

