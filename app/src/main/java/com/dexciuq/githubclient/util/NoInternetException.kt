package com.dexciuq.githubclient.util

import java.lang.Exception

class NoInternetException(message: String) : Exception(message)