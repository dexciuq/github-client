package com.dexciuq.githubclient.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dexciuq.githubclient.api.RetrofitClient
import com.dexciuq.githubclient.model.Repository
import com.dexciuq.githubclient.util.NoInternetException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class MainViewModel(
    private val onStart: () -> Unit,
    private val isNetworkAvailable: () -> Boolean,
    private val onErrorAction: (String) -> Unit,
    private val onEnd: () -> Unit,
) : ViewModel() {

    private val service = RetrofitClient.gitHubApiService

    private val _organization = MutableLiveData<String>()
    val organization : LiveData<String> = _organization

    private val _organizationRepos = MutableLiveData<List<Repository>>()
    val organizationRepos : LiveData<List<Repository>> = _organizationRepos

    fun getOrganizationRepos(org: String) {
        _organization.postValue(org)
        _organizationRepos.postValue(emptyList())
        viewModelScope.launch(Dispatchers.IO) {
            try {
                withContext(Dispatchers.Main) { onStart() }
                if (!isNetworkAvailable()) throw NoInternetException("Action requires internet connection")
                val repos = service.getOrganizationRepos(org)
                _organizationRepos.postValue(repos)
            } catch (e: NoInternetException) {
                withContext(Dispatchers.Main) {
                    onErrorAction("Cannot load repositories")
                }
            } catch (e: HttpException) {
                when (e.code()) {
                    404 -> withContext(Dispatchers.Main) {
                        onErrorAction("No repos found for organization $org")
                    }
                }
            } catch (e: Exception) {
                withContext(Dispatchers.Main) {
                    onErrorAction("Error acquired while loading data")
                }
            } finally {
                withContext(Dispatchers.Main) { onEnd() }
            }
        }
    }
}