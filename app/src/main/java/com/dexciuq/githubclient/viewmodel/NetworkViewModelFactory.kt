package com.dexciuq.githubclient.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

@Suppress("UNCHECKED_CAST")
class NetworkViewModelFactory(
    private val onStart: () -> Unit,
    private val isNetworkAvailable: () -> Boolean,
    private val onErrorAction: (String) -> Unit,
    private val onEnd: () -> Unit,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(onStart, isNetworkAvailable, onErrorAction, onEnd) as T
        }
        if (modelClass.isAssignableFrom(DetailsViewModel::class.java)) {
            return DetailsViewModel(onStart, isNetworkAvailable, onErrorAction, onEnd) as T
        }
        error("unknown viewModel class")
    }
}