package com.dexciuq.githubclient.adapter

import androidx.recyclerview.widget.DiffUtil
import com.dexciuq.githubclient.model.Repository

class RepositoryDiffCallback  : DiffUtil.ItemCallback<Repository>() {

    override fun areItemsTheSame(oldItem: Repository, newItem: Repository): Boolean {
        return oldItem.name == newItem.name
    }

    override fun areContentsTheSame(oldItem: Repository, newItem: Repository): Boolean {
        return oldItem == newItem
    }
}