package com.dexciuq.githubclient.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.dexciuq.githubclient.databinding.ItemLayoutBinding
import com.dexciuq.githubclient.model.Repository

class RepositoryAdapter(
    private val onItemClick: (Repository) -> Unit
) : ListAdapter<Repository, RepositoryAdapter.ViewHolder>(RepositoryDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemLayoutBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(getItem(position))

    inner class ViewHolder(
        private val binding: ItemLayoutBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(repository: Repository) {
            binding.name.text = repository.name
            binding.description.text = repository.description ?: "No description message"
            binding.root.setOnClickListener { onItemClick(repository) }
        }
    }
}